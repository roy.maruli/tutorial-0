package id.ac.ui.cs.advprog.apdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class APdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(APdemoApplication.class, args);
    }

}
